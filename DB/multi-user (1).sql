-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2021 at 02:26 PM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `multi-user`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

CREATE TABLE `aplikasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_owner` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `tlp` varchar(50) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `nama_aplikasi` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `copy_right` varchar(50) DEFAULT NULL,
  `versi` varchar(20) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `nama_owner`, `alamat`, `tlp`, `title`, `nama_aplikasi`, `logo`, `copy_right`, `versi`, `tahun`) VALUES
(1, 'PE IT', 'BEKASI TIMUR', '0812-9936-9059', 'Aplikasi LPM', 'LPM', 'admin1.png', 'Copy Right &copy;', '1.0.0.0', 2022);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_menu`
--

CREATE TABLE `tbl_akses_menu` (
  `id` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_menu`
--

INSERT INTO `tbl_akses_menu` (`id`, `id_level`, `id_menu`, `view_level`) VALUES
(1, 1, 1, 'Y'),
(2, 1, 2, 'Y'),
(43, 4, 1, 'N'),
(44, 4, 2, 'N'),
(64, 1, 52, 'Y'),
(65, 4, 52, 'N'),
(67, 1, 53, 'Y'),
(68, 4, 53, 'Y'),
(70, 1, 54, 'Y'),
(71, 4, 54, 'N'),
(73, 1, 55, 'Y'),
(74, 4, 55, 'N'),
(76, 6, 1, 'Y'),
(77, 6, 2, 'N'),
(78, 6, 52, 'Y'),
(79, 6, 53, 'Y'),
(80, 6, 54, 'Y'),
(81, 6, 55, 'Y'),
(82, 7, 1, 'N'),
(83, 7, 2, 'N'),
(84, 7, 52, 'N'),
(85, 7, 53, 'Y'),
(86, 7, 54, 'N'),
(87, 7, 55, 'N'),
(88, 1, 56, 'Y'),
(89, 4, 56, 'N'),
(90, 6, 56, 'N'),
(91, 7, 56, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_submenu`
--

CREATE TABLE `tbl_akses_submenu` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N',
  `add_level` enum('Y','N') DEFAULT 'N',
  `edit_level` enum('Y','N') DEFAULT 'N',
  `delete_level` enum('Y','N') DEFAULT 'N',
  `print_level` enum('Y','N') DEFAULT 'N',
  `upload_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_submenu`
--

INSERT INTO `tbl_akses_submenu` (`id`, `id_level`, `id_submenu`, `view_level`, `add_level`, `edit_level`, `delete_level`, `print_level`, `upload_level`) VALUES
(2, 1, 2, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(4, 1, 1, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(6, 1, 7, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(7, 1, 8, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(9, 1, 10, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(13, 1, 14, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(26, 1, 15, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(30, 1, 17, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(32, 1, 18, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(34, 1, 19, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(36, 1, 20, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(59, 4, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(60, 4, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(61, 4, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(62, 4, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(63, 4, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(64, 4, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(65, 4, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(66, 4, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(67, 4, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(68, 4, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(82, 1, 23, 'Y', 'N', 'N', 'N', 'N', 'N'),
(83, 4, 23, 'N', 'N', 'N', 'N', 'N', 'N'),
(85, 1, 24, 'Y', 'N', 'N', 'N', 'N', 'N'),
(86, 4, 24, 'Y', 'N', 'N', 'N', 'N', 'N'),
(88, 1, 25, 'Y', 'N', 'N', 'N', 'N', 'N'),
(89, 4, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(91, 1, 26, 'Y', 'N', 'N', 'N', 'N', 'N'),
(92, 4, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(94, 1, 27, 'Y', 'N', 'N', 'N', 'N', 'N'),
(95, 4, 27, 'N', 'N', 'N', 'N', 'N', 'N'),
(97, 1, 28, 'Y', 'N', 'N', 'N', 'N', 'N'),
(98, 4, 28, 'N', 'N', 'N', 'N', 'N', 'N'),
(100, 1, 29, 'Y', 'N', 'N', 'N', 'N', 'N'),
(101, 4, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(103, 1, 30, 'Y', 'N', 'N', 'N', 'N', 'N'),
(104, 4, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(106, 6, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(107, 6, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(108, 6, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(109, 6, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(110, 6, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(111, 6, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(112, 6, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(113, 6, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(114, 6, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(115, 6, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(116, 6, 23, 'Y', 'N', 'N', 'N', 'N', 'N'),
(117, 6, 24, 'Y', 'N', 'N', 'N', 'N', 'N'),
(118, 6, 25, 'Y', 'N', 'N', 'N', 'N', 'N'),
(119, 6, 26, 'Y', 'N', 'N', 'N', 'N', 'N'),
(120, 6, 27, 'Y', 'N', 'N', 'N', 'N', 'N'),
(121, 6, 28, 'Y', 'N', 'N', 'N', 'N', 'N'),
(122, 6, 29, 'Y', 'N', 'N', 'N', 'N', 'N'),
(123, 6, 30, 'Y', 'N', 'N', 'N', 'N', 'N'),
(124, 7, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(125, 7, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(126, 7, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(127, 7, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(128, 7, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(129, 7, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(130, 7, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(131, 7, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(132, 7, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(133, 7, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(134, 7, 23, 'Y', 'N', 'N', 'N', 'N', 'N'),
(135, 7, 24, 'N', 'N', 'N', 'N', 'N', 'N'),
(136, 7, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(137, 7, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(138, 7, 27, 'N', 'N', 'N', 'N', 'N', 'N'),
(139, 7, 28, 'N', 'N', 'N', 'N', 'N', 'N'),
(140, 7, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(141, 7, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(142, 1, 31, 'Y', 'N', 'N', 'N', 'N', 'N'),
(143, 4, 31, 'N', 'N', 'N', 'N', 'N', 'N'),
(144, 6, 31, 'Y', 'N', 'N', 'N', 'N', 'N'),
(145, 7, 31, 'N', 'N', 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_counter`
--

CREATE TABLE `tbl_counter` (
  `id_counter` int(11) NOT NULL,
  `nomor_counter` decimal(6,2) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_counter`
--

INSERT INTO `tbl_counter` (`id_counter`, `nomor_counter`, `created_at`) VALUES
(1, '4.00', '2021-12-08 10:24:23'),
(2, '4.20', '2021-12-08 10:24:29'),
(3, '4.30', '2021-12-08 10:24:53'),
(4, '4.50', '2021-12-08 10:25:08'),
(5, '4.60', '2021-12-08 10:25:19'),
(6, '4.70', '2021-12-08 10:25:31'),
(7, '4.80', '2021-12-08 10:25:42'),
(8, '5.00', '2021-12-08 10:25:51'),
(9, '5.20', '2021-12-08 10:26:02'),
(10, '3.80', '2021-12-16 09:31:49');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_kerusakan`
--

CREATE TABLE `tbl_detail_kerusakan` (
  `id_detail` int(11) NOT NULL,
  `nomor_mesin` varchar(50) NOT NULL,
  `nik` int(11) NOT NULL,
  `kode_dieset` varchar(50) NOT NULL,
  `nama_item` varchar(50) NOT NULL,
  `counter` int(11) NOT NULL,
  `nama_problem` varchar(50) NOT NULL,
  `nama_detil` varchar(50) NOT NULL,
  `nama_masalah` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `sparepart` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detail_kerusakan`
--

INSERT INTO `tbl_detail_kerusakan` (`id_detail`, `nomor_mesin`, `nik`, `kode_dieset`, `nama_item`, `counter`, `nama_problem`, `nama_detil`, `nama_masalah`, `status`, `sparepart`, `created_at`, `update_at`) VALUES
(1, 'A-2', 0, 'D1', '4', 6000, '1', '2', '1', '1', '', '2021-12-14 10:42:21', '2021-12-14 10:42:21'),
(2, 'A-3', 0, 'D', '2', 7600, '1', '1', '3', '1', '', '2021-12-16 13:28:51', '2021-12-16 13:28:51'),
(3, 'A-1', 23211626, 'D1', '4', 6000, '1', '2', '1', '1', '', '2021-12-16 14:49:45', '2021-12-16 14:49:45'),
(4, 'A-2', 23211626, 'D1', '4', 5000, '1', '2', '1', '1', '', '2021-12-16 14:58:53', '2021-12-16 14:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detil_problem`
--

CREATE TABLE `tbl_detil_problem` (
  `id_detil` int(11) NOT NULL,
  `id_problem` int(11) NOT NULL,
  `nama_detil` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detil_problem`
--

INSERT INTO `tbl_detil_problem` (`id_detil`, `id_problem`, `nama_detil`, `created_at`) VALUES
(1, 1, 'Bari', '2021-12-07 16:21:11'),
(2, 1, 'Angular Pin', '2021-12-07 16:22:42'),
(3, 1, 'Beda Tingkat', '2021-12-07 16:22:54'),
(4, 2, 'Bekas Gate', '2021-12-07 16:23:03'),
(5, 2, 'Bocor', '2021-12-07 16:23:11'),
(6, 2, 'Centilan', '2021-12-07 16:23:21'),
(7, 2, 'Check Line', '2021-12-07 16:23:38'),
(8, 1, 'Core', '2021-12-07 16:24:20'),
(9, 1, 'Core Block', '2021-12-07 16:24:26'),
(10, 2, 'Cuci', '2021-12-07 16:24:30'),
(11, 1, 'Cuci Dieset', '2021-12-07 16:24:38'),
(12, 2, 'Dieset', '2021-12-07 16:24:48'),
(13, 1, 'Discharge Alarm', '2021-12-07 16:25:00'),
(15, 4, 'coba', '2021-12-16 09:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dieset`
--

CREATE TABLE `tbl_dieset` (
  `id_dieset` int(11) NOT NULL,
  `kode_dieset` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_dieset`
--

INSERT INTO `tbl_dieset` (`id_dieset`, `kode_dieset`, `created_at`) VALUES
(1, 'D1', '2021-12-08 11:13:22'),
(2, 'D2', '2021-12-08 11:13:34'),
(3, 'D3', '2021-12-08 11:13:43'),
(4, 'D4', '2021-12-08 11:13:50'),
(5, 'D5', '2021-12-08 11:13:59'),
(6, 'D6', '2021-12-08 11:14:09'),
(7, 'D7', '2021-12-08 11:14:18'),
(8, 'D8', '2021-12-08 11:14:33'),
(9, 'D9', '2021-12-08 11:14:53'),
(10, 'D10', '2021-12-16 09:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_item`
--

CREATE TABLE `tbl_item` (
  `id_item` int(11) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `id_counter` int(11) NOT NULL,
  `id_dieset` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_item`
--

INSERT INTO `tbl_item` (`id_item`, `nama_item`, `id_counter`, `id_dieset`, `created_at`) VALUES
(1, '02 SL DF1-300 Z10', 10, 5, '2021-12-08 10:50:47'),
(2, '03 C DA16Q-100 Z100', 0, 0, '2021-12-08 10:50:54'),
(3, '03 C DA3-100 Z', 1, 1, '2021-12-08 10:51:00'),
(4, '03 C DP-100 Z', 1, 1, '2021-12-08 10:51:25'),
(5, '03 EN DP-100 Z', 1, 1, '2021-12-08 10:51:33'),
(6, '05 CN DA8-100 Z', 1, 1, '2021-12-08 10:51:39'),
(7, '05 CN DA86-300 Z', 1, 1, '2021-12-08 10:51:46'),
(8, '05 RC DF9LZ-300 Z', 1, 1, '2021-12-08 10:51:55'),
(9, '05 RC DFB2-100 Z', 1, 1, '2021-12-08 10:52:06'),
(10, '05 RC DFL-100 Z', 1, 1, '2021-12-08 10:52:13'),
(11, '05 RC DFL-300 Z', 1, 1, '2021-12-08 10:52:35'),
(12, '05 RC DFW-300 Z', 1, 1, '2021-12-08 10:52:45'),
(13, '05 VS DA8-100 Z', 1, 1, '2021-12-08 10:52:52'),
(14, '05 VS DA8-900 Z', 1, 1, '2021-12-08 10:53:02'),
(15, '45 C DA-100 Z', 0, 0, '2021-12-08 10:53:10'),
(16, 'cob', 10, 10, '2021-12-16 11:04:31'),
(17, '03 C SABIL-100 Z', 10, 10, '2021-12-16 13:15:40');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE `tbl_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nik` int(11) NOT NULL,
  `nama_karyawan` varchar(255) NOT NULL,
  `job` enum('mekanik','operator') NOT NULL,
  `status` enum('Y','N') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`id_karyawan`, `nik`, `nama_karyawan`, `job`, `status`, `created_at`, `updated_at`) VALUES
(1, 23211626, 'alvin khanh rishad', 'operator', 'Y', '2021-12-13 16:42:45', '2021-12-13 16:42:45'),
(2, 123, 'coba', 'mekanik', 'N', '2021-12-13 16:55:50', '2021-12-16 15:47:41'),
(3, 13123, 'cobadad', 'operator', 'Y', '2021-12-13 16:56:57', '2021-12-13 16:56:57');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kerusakan`
--

CREATE TABLE `tbl_kerusakan` (
  `id_kerusakan` int(11) NOT NULL,
  `nomor_mesin` varchar(30) NOT NULL,
  `nik` int(11) NOT NULL,
  `kode_dieset` varchar(30) NOT NULL,
  `id_item` int(11) NOT NULL,
  `counter` int(11) NOT NULL,
  `id_problem` int(11) NOT NULL,
  `id_detil` int(11) NOT NULL,
  `id_masalah` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `sparepart` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `update_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_kerusakan`
--

INSERT INTO `tbl_kerusakan` (`id_kerusakan`, `nomor_mesin`, `nik`, `kode_dieset`, `id_item`, `counter`, `id_problem`, `id_detil`, `id_masalah`, `status`, `sparepart`, `keterangan`, `created_at`, `update_at`) VALUES
(1, 'A-2', 23211626, 'D1', 2, 6000, 1, 2, 1, 3, 'besi', '', '2021-12-10 00:00:00', '2021-12-16 15:15:30'),
(2, 'A-1', 23211626, 'D1', 4, 5000, 1, 1, 4, 3, 'coba', '', '2021-12-10 15:09:46', '2021-12-16 15:15:34'),
(3, 'A-1', 23211626, 'D1', 8, 500, 1, 1, 3, 3, 'oke', '', '2021-12-10 15:15:05', '2021-12-16 15:15:36'),
(4, 'A-1', 23211626, 'D1', 1, 6000, 1, 2, 1, 3, 'berubah', '', '2021-12-10 16:59:25', '2021-12-16 15:15:39'),
(5, 'A-1', 23211626, 'D1', 2, 5000, 1, 2, 1, 3, 'we', '', '2021-12-13 09:37:15', '2021-12-16 15:15:41'),
(6, 'A-2', 23211626, 'D1', 2, 6000, 1, 2, 1, 3, 'yuhuu', '', '2021-12-13 09:44:00', '2021-12-16 15:15:43'),
(7, 'A-1', 23211626, 'D1', 9, 6000, 1, 2, 1, 3, 'coba', '', '2021-12-13 10:04:47', '2021-12-16 15:15:45'),
(8, 'A-2', 23211626, 'D1', 1, 2321312, 1, 1, 2, 3, 'cobalgi2', '', '2021-12-13 10:05:01', '2021-12-16 15:15:47'),
(9, 'A-2', 23211626, 'D1', 4, 6000, 1, 2, 1, 3, 'besi', '', '2021-12-14 10:41:54', '2021-12-16 15:15:49'),
(10, 'A-2', 23211626, 'D1', 4, 6000, 1, 2, 1, 2, 'bresi', 'cobacoba', '2021-12-14 10:42:21', '2021-12-16 15:46:38'),
(11, 'A-3', 23211626, 'D1', 2, 7600, 1, 1, 3, 3, 'bresi', '', '2021-12-16 13:28:51', '2021-12-16 15:15:53'),
(12, '23211626', 23211626, 'A-1', 0, 4, 6000, 1, 2, 1, '', '', '2021-12-16 14:47:38', '2021-12-16 15:15:55'),
(13, 'A-1', 23211626, 'D1', 4, 6000, 1, 2, 1, 1, '', '', '2021-12-16 14:49:45', '2021-12-16 14:49:45'),
(14, 'A-2', 23211626, 'D1', 4, 5000, 1, 2, 1, 1, '', '', '2021-12-16 14:58:53', '2021-12-16 14:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_masalah`
--

CREATE TABLE `tbl_masalah` (
  `id_masalah` int(11) NOT NULL,
  `id_detil` int(11) NOT NULL,
  `nama_masalah` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_masalah`
--

INSERT INTO `tbl_masalah` (`id_masalah`, `id_detil`, `nama_masalah`, `created_at`) VALUES
(1, 2, 'Anguler Pin Patah', '2021-12-07 16:37:47'),
(2, 1, 'Perb. Bari', '2021-12-07 16:38:38'),
(3, 1, 'Masih Bari Lubang', '2021-12-07 16:39:09'),
(4, 1, 'Bari Samping.  ', '2021-12-07 16:39:23'),
(5, 5, 'mesin mati kurang bahan', '2021-12-09 13:09:48'),
(6, 15, 'MISAL', '2021-12-16 09:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` bigint(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `parent` enum('Y') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `urutan`, `is_active`, `parent`) VALUES
(1, 'Dashboard', 'dashboard', 'fas fa-tachometer-alt', 1, 'Y', 'Y'),
(2, 'System', '#', 'fas fa-cogs', 2, 'Y', 'Y'),
(52, 'Management Data Problem', '#', 'far fa-plus-square', 4, 'Y', 'Y'),
(53, 'Input', '#', 'fas fa-file-upload', 6, 'Y', 'Y'),
(54, 'Management Item', '#', 'far fa-plus-square', 5, 'Y', 'Y'),
(55, 'Data Keseluruhan', 'lihat_dokumen', 'far fa-plus-square', 4, 'Y', 'Y'),
(56, 'Management Karyawan', 'karyawan', 'fas fa-tasks', 4, 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_mesin`
--

CREATE TABLE `tbl_mesin` (
  `id_mesin` int(11) NOT NULL,
  `nama_mesin` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_mesin`
--

INSERT INTO `tbl_mesin` (`id_mesin`, `nama_mesin`, `created_at`) VALUES
(1, 'A-1', '2021-12-09 15:03:14'),
(2, 'A-2', '2021-12-09 15:03:24'),
(3, 'A-3', '2021-12-16 09:47:29');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_problem`
--

CREATE TABLE `tbl_problem` (
  `id_problem` int(11) NOT NULL,
  `nama_problem` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_problem`
--

INSERT INTO `tbl_problem` (`id_problem`, `nama_problem`, `created_at`) VALUES
(1, 'DIESET', '2021-12-07 15:18:58'),
(2, 'MC', '2021-12-07 15:19:39'),
(4, 'bari', '2021-12-16 09:24:36');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_status`
--

CREATE TABLE `tbl_status` (
  `id_status` int(11) NOT NULL,
  `keterangan_status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_status`
--

INSERT INTO `tbl_status` (`id_status`, `keterangan_status`) VALUES
(1, 'rusak'),
(2, 'perbaikan'),
(3, 'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `id_submenu` int(11) UNSIGNED NOT NULL,
  `nama_submenu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`id_submenu`, `nama_submenu`, `link`, `icon`, `id_menu`, `is_active`) VALUES
(1, 'Menu', 'menu', 'far fa-circle', 2, 'Y'),
(2, 'SubMenu', 'submenu', 'far fa-circle', 2, 'Y'),
(7, 'Aplikasi', 'aplikasi', 'far fa-circle', 2, 'Y'),
(8, 'User', 'user', 'far fa-circle', 2, 'Y'),
(10, 'User Level', 'userlevel', 'far fa-circle', 2, 'Y'),
(15, 'Barang', 'barang', 'far fa-circle', 32, 'Y'),
(17, 'Kategori', 'kategori', 'far fa-circle', 32, 'Y'),
(18, 'Satuan', 'satuan', 'far fa-circle', 32, 'Y'),
(19, 'Pembelian', 'pembelian', 'far fa-circle', 41, 'Y'),
(20, 'Penjualan', 'penjualan', 'far fa-circle', 41, 'Y'),
(23, 'Input Perbaikan', 'perbaikan', 'fas fa-file-upload', 53, 'Y'),
(24, 'Input Kerusakan', 'kerusakan', 'far fa-plus-square', 53, 'Y'),
(25, 'Detail Problem', 'detil', 'fas fa-tasks', 52, 'Y'),
(26, 'Problem', 'problem', 'fas fa-tasks', 52, 'Y'),
(27, 'Jenis Masalah', 'jenis_masalah', 'fas fa-tasks', 52, 'Y'),
(28, 'Item', 'item', 'fas fa-tasks', 54, 'Y'),
(29, 'Cycle', 'counter', 'fas fa-tasks', 54, 'Y'),
(30, 'Kode Dieset', 'kdieset', 'far fa-plus-square', 54, 'Y'),
(31, 'Mesin', 'mesin', 'far fa-plus-square', 54, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) UNSIGNED NOT NULL,
  `id_level` int(11) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `id_level`, `username`, `full_name`, `password`, `is_active`) VALUES
(1, 1, 'admin', 'Administratorr', '$2y$05$JgSL8zuc5DIC/0MFLJ9qeewRNp7vyle1DIr6en8MN.xapN4fTQHQ6', 'Y'),
(7, 1, 'alvinkhan', 'alvin khanh rishad', '$2y$05$rwu8.3ag5UUlydIAfhghYe8GqWEKH7ItjEziV5tTWo2tYGNntVgPa', 'Y'),
(20, 7, 'mekanik', 'mekanik', '$2y$05$TW9eciklsls3BbbY8tOOeOfvBLNXivwuquZL07GrzvYzZXuYCkKK.', 'Y'),
(21, 4, 'operator', 'operator', '$2y$05$kWjDuqT8JCxXhvzGLZ3IwOrwLjZd0yCMvvmgskTlrXy1lYTkONQrC', 'Y'),
(22, 6, 'adminpe', 'adminpe', '$2y$05$oz2DrfdDayGc9D11ic4hgOgm.8EP8Qyvjx0PJtyLII4sBSiuB91Ze', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `userlevel`
--

CREATE TABLE `userlevel` (
  `id_level` int(11) UNSIGNED NOT NULL,
  `nama_level` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `userlevel`
--

INSERT INTO `userlevel` (`id_level`, `nama_level`) VALUES
(1, 'Super Admin'),
(4, 'Operator'),
(6, 'Admin'),
(7, 'Mekanik');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  ADD PRIMARY KEY (`id_counter`);

--
-- Indexes for table `tbl_detail_kerusakan`
--
ALTER TABLE `tbl_detail_kerusakan`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `tbl_detil_problem`
--
ALTER TABLE `tbl_detil_problem`
  ADD PRIMARY KEY (`id_detil`);

--
-- Indexes for table `tbl_dieset`
--
ALTER TABLE `tbl_dieset`
  ADD PRIMARY KEY (`id_dieset`);

--
-- Indexes for table `tbl_item`
--
ALTER TABLE `tbl_item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indexes for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `tbl_kerusakan`
--
ALTER TABLE `tbl_kerusakan`
  ADD PRIMARY KEY (`id_kerusakan`);

--
-- Indexes for table `tbl_masalah`
--
ALTER TABLE `tbl_masalah`
  ADD PRIMARY KEY (`id_masalah`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_mesin`
--
ALTER TABLE `tbl_mesin`
  ADD PRIMARY KEY (`id_mesin`);

--
-- Indexes for table `tbl_problem`
--
ALTER TABLE `tbl_problem`
  ADD PRIMARY KEY (`id_problem`);

--
-- Indexes for table `tbl_status`
--
ALTER TABLE `tbl_status`
  ADD PRIMARY KEY (`id_status`);

--
-- Indexes for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `userlevel`
--
ALTER TABLE `userlevel`
  ADD PRIMARY KEY (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplikasi`
--
ALTER TABLE `aplikasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=154;

--
-- AUTO_INCREMENT for table `tbl_counter`
--
ALTER TABLE `tbl_counter`
  MODIFY `id_counter` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_detail_kerusakan`
--
ALTER TABLE `tbl_detail_kerusakan`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_detil_problem`
--
ALTER TABLE `tbl_detil_problem`
  MODIFY `id_detil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_dieset`
--
ALTER TABLE `tbl_dieset`
  MODIFY `id_dieset` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_item`
--
ALTER TABLE `tbl_item`
  MODIFY `id_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  MODIFY `id_karyawan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_kerusakan`
--
ALTER TABLE `tbl_kerusakan`
  MODIFY `id_kerusakan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tbl_masalah`
--
ALTER TABLE `tbl_masalah`
  MODIFY `id_masalah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `tbl_mesin`
--
ALTER TABLE `tbl_mesin`
  MODIFY `id_mesin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_problem`
--
ALTER TABLE `tbl_problem`
  MODIFY `id_problem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_status`
--
ALTER TABLE `tbl_status`
  MODIFY `id_status` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  MODIFY `id_submenu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `userlevel`
--
ALTER TABLE `userlevel`
  MODIFY `id_level` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
