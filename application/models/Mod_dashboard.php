<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_dashboard extends CI_Model
{
    function Jum_masalah()
    {
        $this->db->group_by('id_item');
        $this->db->select('id_masalah');
        $this->db->select("count(*) as total");
        return $this->db->from('tbl_kerusakan')
            ->get()
            ->result();
    }
}
