<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_item extends CI_Model
{
	var $table = 'tbl_item';
	var $column_search = array('a.id_item', 'a.nama_item', 'b.kode_dieset', 'c.nomor_counter', 'a.created_at');
	var $column_order = array('id_item', 'nama_item', 'kode_dieset', 'nomor_counter', 'created_at', null);
	var $order = array('id_item' => 'ascd');
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($term = '')
	{

		$this->db->select('a.*,b.kode_dieset,c.nomor_counter');
		$this->db->join('tbl_counter c', 'a.id_counter=c.id_counter');
		$this->db->join('tbl_dieset b', 'a.id_dieset=b.id_dieset');
		$this->db->from('tbl_item a');
		$this->db->like('a.id_item', $term);
		$this->db->or_like('a.nama_item', $term);
		$this->db->or_like('a.created_at', $term);
		$this->db->or_like('b.kode_dieset', $term);
		$this->db->or_like('c.nomor_counter', $term);

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getAll()
	{
		$this->db->select('a.*,b.kode_dieset,c.nomor_counter');
		$this->db->join('tbl_counter c', 'a.id_counter=c.id_counter');
		$this->db->join('tbl_dieset b', 'a.id_dieset=b.id_dieset');
		$this->db->order_by('a.id_item desc');
		return $this->db->get('tbl_item a');
	}

	function get_datatables()
	{
		$term = $_REQUEST['search']['value'];
		$this->_get_datatables_query($term);
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$term = $_REQUEST['search']['value'];
		$this->_get_datatables_query($term);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->from('tbl_item');
		return $this->db->count_all_results();
	}

	function insert_item($table, $data)
	{
		$insert = $this->db->insert($table, $data);
		return $insert;
	}

	function update_item($id, $data)
	{
		$this->db->where('id_item', $id);
		$this->db->update('tbl_item', $data);
	}

	function get_item($id)
	{
		$this->db->where('id_item', $id);
		return $this->db->get('tbl_item')->row();
	}

	function delete_item($id, $table)
	{
		$this->db->where('id_item', $id);
		$this->db->delete($table);
	}

	function dieset()
	{
		return $this->db->order_by('kode_dieset ASC')
			->get('tbl_dieset')
			->result();
	}

	function counter()
	{
		return $this->db->order_by('nomor_counter ASC')
			->get('tbl_counter')
			->result();
	}
}
