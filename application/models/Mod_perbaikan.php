<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_perbaikan extends CI_Model
{
    var $table = 'tbl_kerusakan';
    var $column_search = array('a.id_kerusakan', 'a.nomor_mesin', 'a.kode_dieset', 'c.nama_masalah', 'f.keterangan_status', 'a.created_at');
    var $column_order = array('id_kerusakan', 'kode_dieset', 'nama_masalah', 'nomor_mesin', 'keterangan_status', 'nama_detil', 'created_at', null);
    var $order = array('id_kerusakan' => 'ascd');
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query($term = '')
    {
        $this->db->select('a.*, c.nama_masalah,f.keterangan_status');
        $this->db->from('tbl_kerusakan a');
        $this->db->join('tbl_masalah c', 'a.id_masalah=c.id_masalah', 'left');
        $this->db->join('tbl_status f', 'a.status=f.id_status', 'left');
        $this->db->where('a.status <=', '2');

        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getAll()
    {
        $this->db->select('a.*, c.nama_masalah,d.nama_detil,f.keterangan_status');
        $this->db->from('tbl_kerusakan a');
        $this->db->join('tbl_masalah c', 'a.id_masalah=c.id_masalah', 'left');
        $this->db->join('tbl_detil_problem d', 'a.id_detil=d.id_detil', 'left');
        $this->db->join('tbl_status f', 'a.status=f.id_status', 'left');
        $this->db->order_by('a.id_kerusakan desc');
        return $this->db->get('tbl_kerusakan a');
    }

    function get_datatables()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $term = $_REQUEST['search']['value'];
        $this->_get_datatables_query($term);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all()
    {
        $this->db->from('tbl_kerusakan');
        return $this->db->count_all_results();
    }

    function update_perbaikan($id, $data)
    {
        $this->db->where('id_kerusakan', $id);
        $this->db->update('tbl_kerusakan', $data);
    }

    function get_perbaikan($id)
    {
        $this->db->where('id_kerusakan', $id);
        return $this->db->get('tbl_kerusakan')->row();
    }
}
