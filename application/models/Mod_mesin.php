<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_mesin extends CI_Model
{
	var $table = 'tbl_mesin';
	var $column_search = array('id_mesin', 'nama_mesin', 'created_at');
	var $column_order = array('id_mesin', 'nama_mesin', 'created_at');
	var $order = array('id_mesin' => 'ascd');
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{

		$this->db->from($this->table);
		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getAll()
	{
		$this->db->select('a');
		$this->db->order_by('a.id_mesin desc');
		return $this->db->get('tbl_mesin a');
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->from('tbl_mesin');
		return $this->db->count_all_results();
	}

	function insert_mesin($table, $data)
	{
		$insert = $this->db->insert($table, $data);
		return $insert;
	}

	function update_mesin($id, $data)
	{
		$this->db->where('id_mesin', $id);
		$this->db->update('tbl_mesin', $data);
	}

	function get_mesin($id)
	{
		$this->db->where('id_mesin', $id);
		return $this->db->get('tbl_mesin')->row();
	}

	function delete_mesin($id, $table)
	{
		$this->db->where('id_mesin', $id);
		$this->db->delete($table);
	}
}
