<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_kerusakan extends CI_Model
{

    function mesin()
    {
        return $this->db->order_by('nama_mesin ASC')
            ->get('tbl_mesin')
            ->result();
    }

    function nik()
    {
        $this->db->where('status', 'Y');
        return $this->db->order_by('nama_karyawan ASC')
            ->get('tbl_karyawan')
            ->result();
    }

    function dieset()
    {
        $this->db->order_by('id_dieset', 'ASC');
        return $this->db->from('tbl_dieset')
            ->get()
            ->result();
    }

    function item($id_dieset)
    {
        $query = $this->db->get_where('tbl_item', array('id_dieset' => $id_dieset));
        return $query;
    }

    function problem()
    {
        $this->db->order_by('id_problem', 'ASC');
        return $this->db->from('tbl_problem')
            ->get()
            ->result();
    }

    function detil($id_problem)
    {
        $query = $this->db->get_where('tbl_detil_problem', array('id_problem' => $id_problem));
        return $query;
    }

    function masalah($id_detil)
    {
        $query = $this->db->get_where('tbl_masalah', array('id_detil' => $id_detil));
        return $query;
    }

    function save_($nik, $nomor_mesin, $kode_dieset, $id_item, $counter, $id_problem, $id_detil, $id_masalah, $status)
    {
        $data = array(
            'nomor_mesin' => $nomor_mesin,
            'nik' => $nik,
            'kode_dieset' => $kode_dieset,
            'id_item' => $id_item,
            'counter' => $counter,
            'id_problem' => $id_problem,
            'id_detil' => $id_detil,
            'id_masalah' => $id_masalah,
            'status' => $status
        );
        $this->db->insert('tbl_kerusakan', $data);
    }

    function save_detail($nik, $nomor_mesin, $kode_dieset, $id_item, $counter, $id_problem, $id_detil, $id_masalah, $status)
    {
        $data = array(
            'nomor_mesin' => $nomor_mesin,
            'nik' => $nik,
            'kode_dieset' => $kode_dieset,
            'nama_item' => $id_item,
            'counter' => $counter,
            'nama_problem' => $id_problem,
            'nama_detil' => $id_detil,
            'nama_masalah' => $id_masalah,
            'status' => $status
        );
        $this->db->insert('tbl_detail_kerusakan', $data);
    }
}
