<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Mod_jenis_masalah extends CI_Model
{
	var $table = 'tbl_masalah';
	var $column_search = array('a.id_masalah', 'a.nama_masalah', 'b.nama_detil');
	var $column_order = array('id_masalah', 'nama_masalah', 'nama_detil', null);
	var $order = array('id_masalah' => 'ascd');
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query($term = '')
	{
		$this->db->from('tbl_masalah a');
		$this->db->select('a.*,b.nama_detil');
		$this->db->join('tbl_detil_problem b', 'a.id_detil=b.id_detil');
		$this->db->like('a.id_masalah', $term);
		$this->db->or_like('a.nama_masalah', $term);
		$this->db->or_like('b.nama_detil', $term);

		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getAll()
	{
		$this->db->select('a.*,b.nama_detil');
		$this->db->join('tbl_detil_problem b', 'a.id_detil=b.id_detil');
		$this->db->order_by('a.id_masalah desc');
		return $this->db->get('tbl_masalah a');
	}

	function get_datatables()
	{
		$term = $_REQUEST['search']['value'];
		$this->_get_datatables_query($term);
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$term = $_REQUEST['search']['value'];
		$this->_get_datatables_query($term);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->from('tbl_masalah');
		return $this->db->count_all_results();
	}

	function insert_jenis($table, $data)
	{
		$insert = $this->db->insert($table, $data);
		return $insert;
	}

	function update_jenis($id, $data)
	{
		$this->db->where('id_masalah', $id);
		$this->db->update('tbl_masalah', $data);
	}

	function get_jenis($id)
	{
		$this->db->where('id_masalah', $id);
		return $this->db->get('tbl_masalah')->row();
	}

	function delete_jenis($id, $table)
	{
		$this->db->where('id_masalah', $id);
		$this->db->delete($table);
	}

	function detil()
	{
		return $this->db->order_by('nama_detil ASC')
			->get('tbl_detil_problem')
			->result();
	}
}
