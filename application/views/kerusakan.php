<style>
	.btn-primary {
		width: 100%;
		font-weight: bold;
	}
</style>

<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<!-- /.card -->
				<div class="card">
					<div class="card-header">
						Masukan Data Kerusakan Mesin
					</div>
					<div class="card-body">
						<form action="<?php echo site_url('kerusakan/save'); ?>" method="POST">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="inputState">Nik Karyawan</label>
									<select id="editable-select" name="nik" class="form-control" required>
										<?php
										foreach ($nik as $ni) { ?>
											<option value="<?= $ni->id_karyawan; ?>"><?= $ni->nik; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="inputState">Nomor Mesin</label>
									<select id="editable-select2" name="nomor_mesin" class="form-control" required>
										<?php
										foreach ($mesin as $m) { ?>
											<option value="<?= $m->id_mesin; ?>"><?= $m->nama_mesin; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Kode Dieset</label>
									<input type="text" id="id_dieset" name="kode_dieset" list="dieset" class="form-control" oninput="console.log(this.value);" autocomplete="on" required>
									<datalist id="dieset">
										<?php
										foreach ($dieset as $d) { ?>
											<option data-value="<?= $d->id_dieset; ?>"><?= $d->kode_dieset; ?></option>
										<?php } ?>
									</datalist>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-8">
									<label for="inputCity">Nama Item</label>
									<select id="id_item" name="id_item" class="form-control" required>
									</select>
								</div>
								<div class="form-group col-md-4">
									<label for="inputState">Counter</label>
									<input type="text" name="counter" id="counter" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" required>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="inputState">Nama Problem</label>
									<select id="id_problem" name="id_problem" class=" form-control" required>
										<option selected disabled>Pilih Nama Problem</option>
										<?php
										foreach ($problem as $p) { ?>
											<option value="<?= $p->id_problem; ?>"><?= $p->nama_problem; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="form-group col-md-3">
									<label for="inputPassword4">Nama Detail</label>
									<select id="id_detil" name="id_detil" class="form-control" required>
									</select>
								</div>
								<div class="form-group col-md-6">
									<label for="inputPassword4">Keterangan Masalah</label>
									<select id="id_masalah" name="id_masalah" class="form-control" required>
										<option selected disabled></option>
									</select>
								</div>
								<input type="text" name="status" value="1" hidden>
							</div>
							<button type="submit" class="btn btn-primary">Kirim</button>
						</form>
					</div>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>


<script>
	// edittable-select
	$('#editable-select').editableSelect();
	$('#editable-select2').editableSelect();

	//datalist 
	const des = Object.getOwnPropertyDescriptor(HTMLInputElement.prototype, 'value');
	Object.defineProperty(HTMLInputElement.prototype, 'value', {
		get: function() {
			const value = des.get.call(this);

			if (this.type === 'text' && this.list) {
				const opt = [].find.call(this.list.options, o => o.value === value);
				return opt ? opt.dataset.value : value;
			}

			return value;
		}
	});

	// relasi combobox
	// item
	$(document).ready(function() {

		$('#id_dieset').change(function() {
			var id = $(this).val();
			$.ajax({
				url: "<?php echo site_url('kerusakan/get_item'); ?>",
				method: "POST",
				data: {
					id: id
				},
				async: true,
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<option value=' + data[i].id_item + '>' + data[i].nama_item + '</option>';
					}
					$('#id_item').html(html);

				}
			});
			return false;
		});

	});
	// Detil
	$(document).ready(function() {

		$('#id_problem').change(function() {
			var id = $(this).val();
			$.ajax({
				url: "<?php echo site_url('kerusakan/get_detil'); ?>",
				method: "POST",
				data: {
					id: id
				},
				async: true,
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<option value=' + data[i].id_detil + '>' + data[i].nama_detil + '</option>';
					}
					$('#id_detil').html(html);

				}
			});
			return false;
		});

	});
	// keterangan maslah
	$(document).ready(function() {

		$('#id_detil').change(function() {
			var id = $(this).val();
			$.ajax({
				url: "<?php echo site_url('kerusakan/get_masalah'); ?>",
				method: "POST",
				data: {
					id: id
				},
				async: true,
				dataType: 'json',
				success: function(data) {

					var html = '';
					var i;
					for (i = 0; i < data.length; i++) {
						html += '<option value=' + data[i].id_masalah + '>' + data[i].nama_masalah + '</option>';
					}
					$('#id_masalah').html(html);

				}
			});
			return false;
		});

	});
</script>