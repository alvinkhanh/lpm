<style>
	.table .thead-light th {
		text-align: center;
	}

	.table td,
	.table th {
		text-align-last: center;
	}
</style>
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<!-- /.card -->
				<div class="card">
					<div class="card-header">
						Input Table Perbaikan Mesin
					</div>
					<div class="card-body">
						<table class="table">
							<thead class="thead-light">
								<tr>
									<th scope="col">No</th>
									<th scope="col">Nomor Mesin</th>
									<th scope="col">Kode Dieset</th>
									<th scope="col">Keterangan Masalah</th>
									<th scope="col">Waktu Kerusakan</th>
									<th scope="col">Waktu perbaikan</th>
									<th scope="col">Status</th>
									<th scope="col">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<?php
									$no = 0;
									foreach ($kerusakan->result() as $row) :
										$no++;
									?>
								<tr>
									<td><?php echo $no; ?></td>
									<td><?php echo $row->nomor_mesin; ?></td>
									<td><?php echo $row->kode_dieset; ?></td>
									<td><?php echo $row->nama_masalah; ?></td>
									<td><?php echo $row->created_at; ?></td>
									<td><?php echo $row->update_at; ?></td>
									<td>
										<h4><span class="badge badge-<?= ($row->status == 3) ? 'success' : 'warning'; ?>"><?= $row->keterangan_status;  ?></span> </h4>
									</td>
									<td>
										<div class="tooltip-demo">
											<a data-toggle="modal" data-target="#modal-edit<?= $row->id_kerusakan; ?>" class="btn btn-warning btn-circle" data-popup="tooltip" data-placement="top" title="Detail Data">Lihat Modal</a>
										</div>
									</td>
								</tr>
							<?php endforeach; ?>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>

<!-- Modal -->
<?php $no = 0;
foreach ($kerusakan->result() as $row) : $no++; ?>
	<div class="row">
		<div id="modal-edit<?= $row->id_kerusakan; ?>" class="modal fade">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="staticBackdropLabel">Detail Data Kerusakan Perbaikan</h5>
					</div>
					<form>
						<div class="modal-body">
							<input type="hidden" readonly value="<?= $row->id_kerusakan; ?>" name="id_kerusakan" class="form-control">
							<div class="container">
								<div class="row">
									<div class="col-sm-6">
										<label class='col-md-3'>Nomor Mesin</label>
										<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->nomor_mesin; ?>" class="form-control" disabled>
									</div>
									<div class="col-sm-6">
										<label class='col-md-3'>Kode Dieset</label>
										<input type="text" name="kode_mesin" autocomplete="off" value="<?= $row->kode_dieset; ?>" class="form-control" disabled>
									</div>
								</div>
								<div class="row">
									<div class="col-sm">
										<div class="form-group">
											<label class='col-md-5'>Nama item</label>
											<input type="text" name="id_item" autocomplete="off" value="<?= $row->nama_item; ?>" class="form-control" disabled>
										</div>
									</div>
									<div class="col-sm">
										<div class="form-group">
											<label class='col-md-5'>Counter</label>
											<input type="text" name="counter" autocomplete="off" value="<?= $row->counter; ?>" class="form-control" disabled>
										</div>
									</div>
									<div class="col-sm">
										<div class="form-group">
											<label class='col-md-3'>Problem</label>
											<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->nama_problem; ?>" class="form-control" disabled>
										</div>
									</div>
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<label class='col'>Detail Problem</label>
									<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->nama_detil; ?>" class="form-control" disabled>
								</div>
								<div class="form-group col-md-2">
									<label class='col'>Status</label>
									<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->keterangan_status; ?>" class="form-control" disabled>
								</div>
								<div class="form-group col-md-4">
									<label class='col'>Sparepart</label>
									<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->sparepart; ?>" class="form-control" disabled>
								</div>
							</div>
							<div class="form-row">
								<span class="border border-secondary" style="width: 100%;">
									<div class="form-row">
										<div class="form-group col-6">
											<label>Waktu Awal Kerusakan</label>
											<input type="text" name="nomor_mesin" autocomplete="off" value="<?= $row->created_at; ?>" class="form-control" disabled>
										</div>
										<div class="form-group col-md-6">
											<label>Waktu Perbaikan</label>
											<input type="text" name="update_at" autocomplete="off" value="<?= $row->update_at; ?>" class="form-control" disabled>
										</div>
									</div>
								</span>
							</div>
						</div>
						<div class="modal-footer">
							<button type="submit" class="btn btn-warning"> Download</button>
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
<?php endforeach; ?>