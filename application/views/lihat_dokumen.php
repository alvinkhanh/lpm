<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header bg-light">
						<h3 class="card-title"> Data Kerusakan</h3>
						<div class="text-right">
							<button id="tableButton2" class="btn btn-sm btn-outline-primary" title="Download PDF"><i class="fas fa-download"></i>PDF</button>
							<a href="<?php echo base_url("lihat_dokumen/export"); ?>" class="btn btn-sm btn-outline-success"><i class="fas fa-download"></i>Excel</a>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="sampleTable2" class="table table-bordered table-striped table-hover">
							<thead class="thead-dark">
								<tr class="bg-info">
									<th scope="col">No</th>
									<th scope="col">Nama Operator</th>
									<th scope="col">Nomor Mesin</th>
									<th scope="col">Nama Item</th>
									<th scope="col">Counter</th>
									<th scope="col">Keterangan Masalah</th>
									<th scope="col">Tanggal Produksi</th>
									<th scope="col">Aksi</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</div>
	<!-- /.container-fluid -->
</section>


<script type="text/javascript">
	var save_method; //for save method string
	var table;

	$(document).ready(function() {

		//datatables
		table = $("#sampleTable2").DataTable({
			"responsive": true,
			"autoWidth": false,
			"language": {
				"sEmptyTable": "Data item Belum Ada"
			},
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url('lihat_dokumen/ajax_list') ?>",
				"type": "POST"
			},
			//Set column definition initialisation properties.
			"columnDefs": [{
					"targets": [-1], //last column
					"render": function(data, type, row) {
						return "<a class=\"btn btn-xs btn-outline-primary\" href=\"javascript:void(0)\" title=\"Edit\" onclick=\"edit_item(" + row[0] + ")\"><i class=\"fas fa-eye\"></i></a>";
					},

					"orderable": false, //set not orderable
				},

			],
		});

		//set input/textarea/select event when change value, remove class error and remove text help block 
		$("input").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
			$(this).removeClass('is-invalid');
		});
		$("textarea").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
			$(this).removeClass('is-invalid');
		});
		$("select").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
			$(this).removeClass('is-invalid');
		});

	});

	function reload_table() {
		table.ajax.reload(null, false); //reload datatable ajax 
	}

	const Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 3000
	});

	function edit_item(id) {
		save_method = 'update';
		$('#form')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string

		//Ajax Load data from ajax
		$.ajax({
			url: "<?php echo site_url('lihat_dokumen/lihat') ?>/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data) {

				$('[name="id"]').val(data.id_kerusakan);
				$('[name="nomor_mesin"]').val(data.nomor_mesin);
				$('[name="kode_dieset"]').val(data.kode_dieset);
				$('[name="nama_item"]').val(data.nama_item);
				$('[name="counter"]').val(data.counter);
				$('[name="id_problem"]').val(data.nama_problem);
				$('[name="id_detil"]').val(data.nama_detil);
				$('[name="id_masalah"]').val(data.nama_masalah);
				$('[name="status"]').val(data.keterangan_status);
				$('[name="sparepart"]').val(data.sparepart);
				$('[name="keterangan"]').val(data.keterangan);
				$('[name="created_at"]').val(data.created_at);
				$('[name="update_at"]').val(data.update_at);
				$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
				$('.modal-title').text('Conclusion'); // Set title to Bootstrap modal title

			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Error get data from ajax');
			}
		});
	}
</script>



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content ">
			<div class="modal-header">
				<h3 class="modal-title"></h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body form">
				<input type="hidden" value="" name="id" />
				<form action="#" id="form" class="form-horizontal">
					<div id="app">
						<table id="sampleTable" class="table table-striped">
							<colgroup>
								<col width="20%">
								<col width="20%">
							</colgroup>
							<thead>
								<tr class='warning'>
									<th>Judul</th>
									<th>Keterangan</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td> <label>Nomor Mesin</label> </td>
									<td><input type="text" name="nomor_mesin" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Kode Dieset</label> </td>
									<td><input type="text" name="kode_dieset" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Nama Item</label> </td>
									<td><input type="text" name="nama_item" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Counter</label> </td>
									<td><input type="text" name="counter" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Problem</label> </td>
									<td><input type="text" name="id_problem" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Detil Problem</label> </td>
									<td><input type="text" name="id_detil" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Keterangan Masalah</label> </td>
									<td><input type="text" name="id_masalah" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Sparepart</label> </td>
									<td><input type="text" name="sparepart" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Keterangan Perbaikan</label> </td>
									<td> <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1" rows="3" disabled></textarea> <span class="help-block"></span></td>
								</tr>

								<tr>
									<td> <label>Waktu Kerusakan</label> </td>
									<td><input type="text" name="created_at" class="form-control" disabled></td>
								</tr>
								<tr>
									<td> <label>Waktu Perbaikan</label> </td>
									<td><input type="text" name="update_at" class="form-control" disabled></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button id="tableButton" class="btn btn-primary">PDF</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.1.1/jspdf.plugin.autotable.min.js"></script>
<script>
	$(function() {
		console.log($("#tableButton2"));
		$("#tableButton2").on("click", function() {
			var doc = new jsPDF('p', 'mm', 'a4');
			console.log("Save Button");
			// doc.text(20, 25, "Sample Data");
			doc.autoTable({
				html: $("#sampleTable2").get(0),
				headStyles: {
					halign: "center",
					valign: "middle",
					lineWidth: 0.25,
					lineColor: 200
				},
				bodyStyles: {
					halign: "center",
					lineWidth: 0.25,
					lineColor: 200
				},
			});
			doc.save("sample_kerusakan.pdf");
		});
	});
</script>