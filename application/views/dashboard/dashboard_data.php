<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-lg-3 col-6">
    <!-- small box -->
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <!-- <div class="small-box bg-success">
      <div class="inner">
        <h3>1</h3>
        <p>Angular Pint Patah</p>
      </div>
      <div class="icon">
        <i class="ion ion-stats-bars"></i>
      </div>
    </div> -->
  </div>
  <!-- ./col -->
  <!-- small box -->
  <!-- ./col -->
  <div class="col-lg col-6">
    <!-- <canvas id="myChart"></canvas> -->
    <?php
    //Inisialisasi nilai variabel awal
    $id_masalah = "";
    $jumlah = null;
    foreach ($hasil as $item) {
      $jur = $item->id_masalah;
      $id_masalah .= "'$jur'" . ", ";
      $jum = $item->total;
      $jumlah .= "$jum" . ", ";
    }
    ?>
  </div>

  <canvas id="myChart2" style="width:100%;max-width:600px"></canvas>
  <!-- ./col -->
</div>
<!-- /.row -->
<script>
  var ctx = document.getElementById('myChart').getContext('2d');
  var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'bar',
    // The data for our dataset
    data: {
      labels: [<?php echo $id_masalah; ?>],
      datasets: [{
        label: 'Data Jumlah Masalah ',
        backgroundColor: ['rgb(255, 99, 132)', 'rgba(56, 86, 255, 0.87)', 'rgb(60, 179, 113)', 'rgb(175, 238, 239)'],
        borderColor: ['rgb(255, 99, 132)'],
        data: [<?php echo $jumlah; ?>]
      }]
    },
    // Configuration options go here
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>