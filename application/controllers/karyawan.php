<?php
defined('BASEPATH') or exit('No direct script access allowed');

class karyawan extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_karyawan'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'karyawan');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_karyawan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_karyawan; //array 0
            $row[] = $pel->nik; //array 1
            $row[] = $pel->nama_karyawan; //array 1
            $row[] = $pel->job; //array 1
            $row[] = $pel->created_at; //array 1
            $row[] = $pel->status; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_karyawan->count_all(),
            "recordsFiltered" => $this->Mod_karyawan->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // $this->_validate();
        $save  = array(
            'nik' => $this->input->post('nik'),
            'nama_karyawan' => $this->input->post('nama_karyawan'),
            'job' => $this->input->post('job'),
            'status' => $this->input->post('status')
        );
        $this->Mod_karyawan->insert_karyawan("tbl_karyawan", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nik' => $this->input->post('nik'),
            'nama_karyawan' => $this->input->post('nama_karyawan'),
            'job' => $this->input->post('job'),
            'status' => $this->input->post('status')
        );
        $this->Mod_karyawan->update_karyawan($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_karyawan($id)
    {
        $data = $this->Mod_karyawan->get_karyawan($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_karyawan->delete_karyawan($id, 'tbl_karyawan');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_karyawan') == '') {
            $data['inputerror'][] = 'nama_karyawan';
            $data['error_string'][] = 'Nama karyawan Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
