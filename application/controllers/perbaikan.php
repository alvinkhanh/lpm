<?php
defined('BASEPATH') or exit('No direct script access allowed');

class perbaikan extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_perbaikan'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'perbaikan');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_perbaikan->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->nomor_mesin; //array 1
            $row[] = $pel->kode_dieset; //array 1
            $row[] = $pel->nama_masalah; //array 1
            $row[] = $pel->created_at;
            $row[] = $pel->keterangan_status;
            $row[] = $pel->id_kerusakan; //array 0
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_perbaikan->count_all(),
            "recordsFiltered" => $this->Mod_perbaikan->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }
    //update to database
    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'sparepart' => $this->input->post('sparepart'),
            'keterangan' => $this->input->post('keterangan'),
            'status' => $this->input->post('status')
        );
        $this->Mod_perbaikan->update_perbaikan($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_perbaikan($id)
    {
        $data = $this->Mod_perbaikan->get_perbaikan($id);
        echo json_encode($data);
    }

    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('sparepart') == '') {
            $data['inputerror'][] = 'sparepart';
            $data['error_string'][] = 'sparepart Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($this->input->post('status') == '') {
            $data['inputerror'][] = 'status';
            $data['error_string'][] = 'status Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
