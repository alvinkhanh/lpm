<?php
defined('BASEPATH') or exit('No direct script access allowed');

class counter extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_counter'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'counter');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_counter->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_counter; //array 0
            $row[] = $pel->nomor_counter; //array 1
            $row[] = $pel->created_at; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_counter->count_all(),
            "recordsFiltered" => $this->Mod_counter->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nomor_counter' => $this->input->post('nomor_counter')
        );
        $this->Mod_counter->insert_counter("tbl_counter", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nomor_counter' => $this->input->post('nomor_counter')
        );
        $this->Mod_counter->update_counter($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_counter($id)
    {
        $data = $this->Mod_counter->get_counter($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_counter->delete_counter($id, 'tbl_counter');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nomor_counter') == '') {
            $data['inputerror'][] = 'nomor_counter';
            $data['error_string'][] = 'Nomor counter Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
