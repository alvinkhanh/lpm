<?php
defined('BASEPATH') or exit('No direct script access allowed');

class item extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_item'));
    }

    public function index()
    {
        $this->load->helper('url');
        $data['dieset'] = $this->Mod_item->dieset();
        $data['counter'] = $this->Mod_item->counter();
        $this->template->load('layoutbackend', 'item', $data);
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_item->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_item; //array 0
            $row[] = $pel->nama_item; //array 1
            $row[] = $pel->nomor_counter; //array 1
            $row[] = $pel->kode_dieset; //array 1
            $row[] = $pel->created_at; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_item->count_all(),
            "recordsFiltered" => $this->Mod_item->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // $this->_validate();
        $save  = array(
            'nama_item' => $this->input->post('nama_item'),
            'id_dieset' => $this->input->post('id_dieset'),
            'id_counter' => $this->input->post('id_counter')
        );
        $this->Mod_item->insert_item("tbl_item", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_item' => $this->input->post('nama_item'),
            'id_counter' => $this->input->post('id_counter'),
            'id_dieset' => $this->input->post('id_dieset')
        );
        $this->Mod_item->update_item($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_item($id)
    {
        $data = $this->Mod_item->get_item($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_item->delete_item($id, 'tbl_item');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_item') == '') {
            $data['inputerror'][] = 'nama_item';
            $data['error_string'][] = 'Nama item Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
