<?php
defined('BASEPATH') or exit('No direct script access allowed');


class jenis_masalah extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_jenis_masalah'));
    }

    public function index()
    {
        $this->load->helper('url');
        $data['jenis_dokumen'] = $this->Mod_jenis_masalah->getAll();
        $data['detil'] = $this->Mod_jenis_masalah->detil();
        $this->template->load('layoutbackend', 'tambah_jenis', $data);
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_jenis_masalah->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_masalah; //array 0
            $row[] = $pel->nama_masalah; //array 1
            $row[] = $pel->nama_detil;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_jenis_masalah->count_all(),
            "recordsFiltered" => $this->Mod_jenis_masalah->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nama_masalah' => $this->input->post('nama_masalah'),
            'id_detil' => $this->input->post('id_detil')
        );
        $this->Mod_jenis_masalah->insert_jenis("tbl_masalah", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_masalah' => $this->input->post('nama_masalah'),
            'id_detil' => $this->input->post('id_detil')
        );
        $this->Mod_jenis_masalah->update_jenis($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_jenis($id)
    {
        $data = $this->Mod_jenis_masalah->get_jenis($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_jenis_masalah->delete_jenis($id, 'tbl_masalah');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_masalah') == '') {
            $data['inputerror'][] = 'nama_masalah';
            $data['error_string'][] = 'Nama masalah Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
