<?php
defined('BASEPATH') or exit('No direct script access allowed');


class kdieset extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_kdieset'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'dieset');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_kdieset->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_dieset; //array 0
            $row[] = $pel->kode_dieset; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_kdieset->count_all(),
            "recordsFiltered" => $this->Mod_kdieset->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // $this->_validate();
        $save  = array(
            'kode_dieset' => $this->input->post('kode_dieset')
        );
        $this->Mod_kdieset->insert_dieset("tbl_dieset", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        // $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'kode_dieset' => $this->input->post('kode_dieset')
        );
        $this->Mod_kdieset->update_dieset($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_dieset($id)
    {
        $data = $this->Mod_kdieset->get_dieset($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_kdieset->delete_dieset($id, 'tbl_dieset');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('kode_dieset') == '') {
            $data['inputerror'][] = 'kode_dieset';
            $data['error_string'][] = 'Kode Dieset Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
