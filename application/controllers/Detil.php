<?php
defined('BASEPATH') or exit('No direct script access allowed');

class detil extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_detil'));
    }

    public function index()
    {
        $this->load->helper('url');
        $data['problem'] = $this->Mod_detil->problem();
        $this->template->load('layoutbackend', 'detil', $data);
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_detil->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_detil; //array 0
            $row[] = $pel->nama_detil; //array 1
            $row[] = $pel->nama_problem;
            $row[] = $pel->created_at; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_detil->count_all(),
            "recordsFiltered" => $this->Mod_detil->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $save  = array(
            'nama_detil' => $this->input->post('nama_detil'),
            'id_problem' => $this->input->post('id_problem')
        );
        $this->Mod_detil->insert_detil("tbl_detil_problem", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_detil' => $this->input->post('nama_detil'),
            'id_problem' => $this->input->post('id_problem')
        );
        $this->Mod_detil->update_detil($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_detil($id)
    {
        $data = $this->Mod_detil->get_detil($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_detil->delete_detil($id, 'tbl_detil_problem');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_detil') == '') {
            $data['inputerror'][] = 'nama_detil';
            $data['error_string'][] = 'Nama detil Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
