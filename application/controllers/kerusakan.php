<?php
defined('BASEPATH') or exit('No direct script access allowed');

class kerusakan extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_kerusakan');
    }

    public function index()
    {
        $this->load->helper('url');
        $data['nik'] = $this->Mod_kerusakan->nik();
        $data['mesin'] = $this->Mod_kerusakan->mesin();
        $data['dieset'] = $this->Mod_kerusakan->dieset();
        $data['problem'] = $this->Mod_kerusakan->problem();
        $this->template->load('layoutbackend', 'kerusakan', $data);
    }

    function get_item()
    {
        $id_dieset = $this->input->post('id', TRUE);
        $data = $this->Mod_kerusakan->item($id_dieset)->result();
        echo json_encode($data);
    }

    function get_detil()
    {
        $id_problem = $this->input->post('id', TRUE);
        $data = $this->Mod_kerusakan->detil($id_problem)->result();
        echo json_encode($data);
    }

    function get_masalah()
    {
        $id_detil = $this->input->post('id', TRUE);
        $data = $this->Mod_kerusakan->masalah($id_detil)->result();
        echo json_encode($data);
    }

    //save  to database
    function save()
    {
        $nik     = $this->input->post('nik', TRUE);
        $nomor_mesin     = $this->input->post('nomor_mesin', TRUE);
        $kode_dieset     = $this->input->post('kode_dieset', TRUE);
        $id_item = $this->input->post('id_item', TRUE);
        $counter     = $this->input->post('counter', TRUE);
        $id_problem     = $this->input->post('id_problem', TRUE);
        $id_detil     = $this->input->post('id_detil', TRUE);
        $id_masalah     = $this->input->post('id_masalah', TRUE);
        $status     = $this->input->post('status', TRUE);
        $this->Mod_kerusakan->save_($nik, $nomor_mesin, $kode_dieset, $id_item, $counter, $id_problem, $id_detil, $id_masalah, $status);
        $this->Mod_kerusakan->save_detail($nik, $nomor_mesin, $kode_dieset, $id_item, $counter, $id_problem, $id_detil, $id_masalah, $status);
        $this->session->set_flashdata('msg', '<div class="alert alert-success">Report Send</div>');
        redirect('kerusakan');
    }
    // private function _validate()
    // {
    //     $data = array();
    //     $data['error_string'] = array();
    //     $data['inputerror'] = array();
    //     $data['status'] = TRUE;

    //     if ($this->input->post('nomor_mesin') == '') {
    //         $data['inputerror'][] = 'nomor_mesin';
    //         $data['error_string'][] = 'nomor_mesin is required';
    //         $data['status'] = FALSE;
    //     }

    //     if ($this->input->post('kode_dieset') == '') {
    //         $data['inputerror'][] = 'kode_dieset';
    //         $data['error_string'][] = 'Kode Dieset is required';
    //         $data['status'] = FALSE;
    //     }

    //     if ($this->input->post('id_item') == '') {
    //         $data['inputerror'][] = 'id_item';
    //         $data['error_string'][] = 'Nama Item is required';
    //         $data['status'] = FALSE;
    //     }

    //     if ($this->input->post('counter') == '') {
    //         $data['inputerror'][] = 'counter';
    //         $data['error_string'][] = 'Please Input Counter';
    //         $data['status'] = FALSE;
    //     }

    //     if ($this->input->post('id_problem') == '') {
    //         $data['inputerror'][] = 'id_problem';
    //         $data['error_string'][] = 'Please select id_problem';
    //         $data['status'] = FALSE;
    //     }

    //     if ($data['status'] === FALSE) {
    //         echo json_encode($data);
    //         exit();
    //     }
    // }
}
