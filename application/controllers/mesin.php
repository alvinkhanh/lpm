<?php
defined('BASEPATH') or exit('No direct script access allowed');

class mesin extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_mesin'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'mesin');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_mesin->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_mesin; //array 0
            $row[] = $pel->nama_mesin; //array 1
            $row[] = $pel->created_at; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_mesin->count_all(),
            "recordsFiltered" => $this->Mod_mesin->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $save  = array(
            'nama_mesin' => $this->input->post('nama_mesin')
        );
        $this->Mod_mesin->insert_mesin("tbl_mesin", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_mesin' => $this->input->post('nama_mesin')
        );
        $this->Mod_mesin->update_mesin($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_mesin($id)
    {
        $data = $this->Mod_mesin->get_mesin($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_mesin->delete_mesin($id, 'tbl_mesin');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_mesin') == '') {
            $data['inputerror'][] = 'nama_mesin';
            $data['error_string'][] = 'Nama mesin Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
