<?php
defined('BASEPATH') or exit('No direct script access allowed');


class problem extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_problem'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'problem');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_problem->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_problem; //array 0
            $row[] = $pel->nama_problem; //array 1
            $row[] = $pel->created_at; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_problem->count_all(),
            "recordsFiltered" => $this->Mod_problem->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nama_problem' => $this->input->post('nama_problem')
        );
        $this->Mod_problem->insert_problem("tbl_problem", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_problem' => $this->input->post('nama_problem')
        );
        $this->Mod_problem->update_problem($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_problem($id)
    {
        $data = $this->Mod_problem->get_problem($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_problem->delete_problem($id, 'tbl_problem');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_problem') == '') {
            $data['inputerror'][] = 'nama_problem';
            $data['error_string'][] = 'Nama problem Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
